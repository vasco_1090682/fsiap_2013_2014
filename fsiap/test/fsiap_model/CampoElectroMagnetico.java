package fsiap_model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import core.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class CampoElectroMagnetico {
    
    public CampoElectroMagnetico() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
    public void testCalculaRaio() {
        System.out.println("CalculaRaio");
        CampoEletroMagnetico cm = new CampoEletroMagnetico(5, 0.95, 1);
        System.out.println("Calculos Testes Unitários");
        double expResultRaio = 7.95 * Math.pow(10, -6);
        // double expResult = 79.37115005551876 * Math.pow(10, -6);
        double resultRaio = cm.calculaRaio();
        assertEquals(expResultRaio, resultRaio, 0);


        double expResultVel = 1326214.35;
        double ResultVel = cm.calcularVelocidade();
        assertEquals(expResultVel, ResultVel, 0);


        double expResultFM = 1.52209 * Math.pow(10, -19);
        double ResultFM = cm.calcularForcaMagnetica();
        assertEquals(expResultFM, ResultFM, 0.00005);
    }
}

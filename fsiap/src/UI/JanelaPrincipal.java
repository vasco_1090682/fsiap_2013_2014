package UI;

import Persistencia.ConexaoBD;
import core.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
import java.awt.geom.QuadCurve2D;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.*;

public class JanelaPrincipal extends JFrame {

    private ConexaoBD conn = new ConexaoBD();
    private JSpinner spinIntensidade, spinDifPotencial, spinIntensidadeMagnetico;
    private JComboBox<String> cmbDirecaoCampoMagnetico;
    private JCheckBox cbActivarCampoMagnetico, cbMostarCampoMagnetico;
    private JPanel panelFeixe;
    private JButton btGravar;
    private int numElectroesFeixe;
    private float velocidadeFeixe = 0f;
    private float velocidadeFeixeInvertida = 0f;
    private final Timer timer;
    int difPotencial;
    
    private int cx = 250; //cateto no eixo dos xx, sendo o comprimento da região do campo magnético
    private int yFimCurva = 0;
    private double intensidadeCampoMagnetico;
    private boolean activarBobines;

    private boolean direcaoCampoMagnetico; //direção do campo magnético: true = positivo, false = negativo

    private boolean mostrarCampoMagnetico = false; //true = mostra o campo, false = não mostra o campo

    private CampoEletroMagnetico campoM;

    //Labels dos dados a apresentar no ecra
    JLabel lbvel2, lbraio2, lbfm2, lbx2, lby2;
    
    //Fim labels dos dados a apresentar no ecra
    
    public JanelaPrincipal(String intensidade,String ddp,String intensidadeCM,String velocidade,String raio,String forcaMagnetica,boolean bobi,boolean dire) {
        super("FSIAP - Tubos Raios Catódicos");
        numElectroesFeixe = Integer.parseInt(intensidade);
        difPotencial = Integer.parseInt(ddp);
        intensidadeCampoMagnetico=Double.parseDouble(intensidadeCM);
        CampoEletroMagnetico cm = new CampoEletroMagnetico(5,intensidadeCampoMagnetico,1);
        this.campoM = cm;
        activarBobines=bobi;
        direcaoCampoMagnetico=dire;
        final Container c = getContentPane();

        //Paineis
        JPanel p = new JPanel(new BorderLayout()); //JPanel principal

        //Panel com o feixe e cargas
        JPanel p1 = new JPanel(new BorderLayout());
        p1.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.black));

        //Panel com o feixe e campo eléctrico para acelarar o feixe
        panelFeixe = new TesteGraphics2d();
        panelFeixe.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY));
        p1.add(panelFeixe, BorderLayout.CENTER);

        p.add(p1, BorderLayout.CENTER);

        //Panel com botões de configuração
        JPanel p2 = new JPanel(new BorderLayout());
        p2.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        //Panel configuração feixe
        JPanel p2_1 = new JPanel(new BorderLayout());
        p2_1.setBorder(BorderFactory.createTitledBorder("Feixe"));

        JPanel p2_1_1 = new JPanel();
        JLabel lbIntensidade = new JLabel("Intensidade:");
        SpinnerModel smIntensidade = new SpinnerNumberModel(numElectroesFeixe, 10, 25, 1); //valor por defeito,valor mínimo,valor máximo,valor incrementação/decrementação
        spinIntensidade = new JSpinner(smIntensidade);
        p2_1_1.add(lbIntensidade);
        p2_1_1.add(spinIntensidade);

        JPanel p2_1_2 = new JPanel();
        String myString1 = "Diferença de potencial:";
        JLabel lbDifPotencial = new JLabel("<html><center>" + myString1 + "</center></html>");
        lbDifPotencial.setPreferredSize(new Dimension(74, 40));
        SpinnerModel smDifPotencial = new SpinnerNumberModel(difPotencial, 1, 10, 1); //valor por defeito,valor mínimo,valor máximo,valor incrementação/decrementação
        spinDifPotencial = new JSpinner(smDifPotencial);
        JLabel lbUniDifP = new JLabel("V");
        p2_1_2.add(lbDifPotencial);
        p2_1_2.add(spinDifPotencial);
        p2_1_2.add(lbUniDifP);

        p2_1.add(p2_1_1, BorderLayout.NORTH);
        p2_1.add(p2_1_2, BorderLayout.CENTER);

        p2.add(p2_1, BorderLayout.NORTH);
        //fim panel configuração feixe e campo eléctrico para acelarar o feixe

        //Panel configuração campo magnético
        JPanel p2_2 = new JPanel();
        p2_2.setBorder(BorderFactory.createTitledBorder("Campo Mágnetico"));

        //Check box
        JPanel p2_2_1 = new JPanel(new BorderLayout());
        cbActivarCampoMagnetico = new JCheckBox("Bobines");
        cbMostarCampoMagnetico = new JCheckBox("Mostrar campo");
        cbMostarCampoMagnetico.setEnabled(false);
        
        p2_2_1.add(cbActivarCampoMagnetico, BorderLayout.NORTH);
        p2_2_1.add(cbMostarCampoMagnetico, BorderLayout.CENTER);

        JPanel p2_2_2 = new JPanel();
        JLabel lbIntensidadeMagnetico = new JLabel(" Intensidade: ");
        SpinnerModel smIntensidadeMagnetico = new SpinnerNumberModel(intensidadeCampoMagnetico, 0.8, 1, 0.01); //valor por defeito,valor mínimo,valor máximo,valor incrementação/decrementação
        spinIntensidadeMagnetico = new JSpinner(smIntensidadeMagnetico);
        spinIntensidadeMagnetico.setEnabled(false);
        JLabel lbUniIntCampM = new JLabel("T");
        p2_2_2.add(lbIntensidadeMagnetico);
        p2_2_2.add(spinIntensidadeMagnetico);
        p2_2_2.add(lbUniIntCampM);

        JPanel p2_2_3 = new JPanel();
        JLabel lbDirecao = new JLabel("  Direção:  ");
        cmbDirecaoCampoMagnetico = new JComboBox<String>();
        cmbDirecaoCampoMagnetico.addItem("Positivo");
        cmbDirecaoCampoMagnetico.addItem("Negativo");
        cmbDirecaoCampoMagnetico.setEnabled(false);
        p2_2_3.add(lbDirecao);
        p2_2_3.add(cmbDirecaoCampoMagnetico);

        
        
        JPanel p2_2_aux = new JPanel(new GridLayout(3, 1)); //painel para limitar o p2_2 ao uma área mais pequena em vez de ocupar a área até ao fim do p2 

        p2_2_aux.add(p2_2_1);
        p2_2_aux.add(p2_2_2);
        p2_2_aux.add(p2_2_3);
        p2_2.add(p2_2_aux);

        p2.add(p2_2, BorderLayout.CENTER);
        //fimPanel configuração campo magnético
        
        //mostrar dados
        JPanel p2_3 = new JPanel(new GridLayout(6,1));
        p2_3.setBorder(BorderFactory.createTitledBorder("Dados"));
        //Velocidade
        JPanel p2_3_1 = new JPanel();
        JLabel lbvel1 = new JLabel("Velocidade:");
        lbvel2 = new JLabel(""+campoM.calcularVelocidade());
        JLabel lbUniVel = new JLabel("m/s");
        p2_3_1.add(lbvel1);
        p2_3_1.add(lbvel2);
        p2_3_1.add(lbUniVel);
        
        //Raio
        JPanel p2_3_2 = new JPanel();
        JLabel lbraio1 = new JLabel("Raio:");
        lbraio2 = new JLabel(""+campoM.calculaRaio());
        JLabel uniRaio = new JLabel("m");
        p2_3_2.add(lbraio1);
        p2_3_2.add(lbraio2);
        p2_3_2.add(uniRaio);
        
        //Força magnética
        JPanel p2_3_3 = new JPanel();
        JLabel lbfm1 = new JLabel("Força magnética:");
        lbfm2 = new JLabel(""+campoM.calcularForcaMagnetica());
        JLabel lbUniFM = new JLabel("N");
        p2_3_3.add(lbfm1);
        p2_3_3.add(lbfm2);
        p2_3_3.add(lbUniFM);
        
        //X
        JPanel p2_3_4 = new JPanel();
        JLabel lbx1 = new JLabel("X:");
        lbx2 = new JLabel(""+cx);
        p2_3_4.add(lbx1);
        p2_3_4.add(lbx2);
        
        //Y
        JPanel p2_3_5 = new JPanel();
        JLabel lby1 = new JLabel("Y:");
        lby2 = new JLabel(""+yFimCurva);
        p2_3_5.add(lby1);
        p2_3_5.add(lby2);

        if (bobi==true)
        {
            spinIntensidadeMagnetico.setEnabled(true);
            cmbDirecaoCampoMagnetico.setEnabled(true);
            cbActivarCampoMagnetico.setSelected(true);
            cbMostarCampoMagnetico.setEnabled(true);
            if(direcaoCampoMagnetico==true)
                cmbDirecaoCampoMagnetico.setSelectedIndex(0);
            else
                cmbDirecaoCampoMagnetico.setSelectedIndex(1);
            c.repaint();
        }
        
        //botão gravar resultados na bd
        btGravar = new JButton("Gravar resultado");
        btGravar.setPreferredSize(new Dimension(180, 25));

        p2_3.add(p2_3_1);
        p2_3.add(p2_3_2);
        p2_3.add(p2_3_3);
        p2_3.add(p2_3_4);
        p2_3.add(p2_3_5);
        p2_3.add(btGravar);
        p2.add(p2_3, BorderLayout.SOUTH);
        //fim mostrar dados
        //fim panel configuração campos

        p.add(p2, BorderLayout.EAST);

        c.add(p);

        //Barra menus
        JMenuBar menuBar = new JMenuBar();
        JMenu menu;
        JMenuItem menuItem;
        JMenu subMenu;

        //-----Menu Ficheiro-----
        menu = new JMenu("Ficheiro");
        menu.setMnemonic('F');
        menuBar.add(menu);

        menuItem = new JMenuItem("Sair", 'S');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fechar();
            }
        });
        menu.add(menuItem);
        
        //-----Menu Base de Dados-----
        menu = new JMenu("Base de Dados");
        menu.setMnemonic('D');
        menuBar.add(menu);

        menuItem = new JMenuItem("Base de Dados", 'B');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl B"));
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BaseDadosUI bd = new BaseDadosUI(JanelaPrincipal.this);
            }
        });
        menu.add(menuItem);

        //-----Menu Ajuda-----
        menu = new JMenu("Ajuda");
        menu.setMnemonic('A');
        menuBar.add(menu);

        menuItem = new JMenuItem("Acerca", 'C');
        menuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl C"));
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AcercaUI p = new AcercaUI(JanelaPrincipal.this);
            }
        });
        menu.add(menuItem);
        setJMenuBar(menuBar);

        //Quando carrega no X, pergunta se realmente desaje fechar
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });

        //Eventos
        spinIntensidade.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                numElectroesFeixe = (Integer) spinIntensidade.getValue();
                c.repaint(); //actualiza a janela dinamicamente
            }
        });

        spinDifPotencial.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                difPotencial = (Integer) spinDifPotencial.getValue();
                campoM.setDifPotencial(difPotencial);
                actualizarLabelsDados();
                c.repaint(); //actualiza a janela dinamicamente
            }
        });

        spinIntensidadeMagnetico.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                intensidadeCampoMagnetico = (Double) spinIntensidadeMagnetico.getValue();
                campoM.setIntensidadeCampo(intensidadeCampoMagnetico);
                actualizarLabelsDados();
                c.repaint(); //actualiza a janela dinamicamente
            }
        });

        cbActivarCampoMagnetico.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if (cbActivarCampoMagnetico.isSelected()) {
                    activarBobines = true;
                    spinIntensidadeMagnetico.setEnabled(true);
                    cmbDirecaoCampoMagnetico.setEnabled(true);
                    cbMostarCampoMagnetico.setEnabled(true);
                    c.repaint();
                } else {
                    activarBobines = false;
                    spinIntensidadeMagnetico.setEnabled(false);
                    cmbDirecaoCampoMagnetico.setEnabled(false);
                    cbMostarCampoMagnetico.setEnabled(false);
                    yFimCurva =0;
                    c.repaint();
                }
                actualizarLabelsDados();
            }
        });
        
        cbMostarCampoMagnetico.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                if (cbMostarCampoMagnetico.isSelected()) {
                    mostrarCampoMagnetico = true;
                    c.repaint();
                } else {
                    mostrarCampoMagnetico = false;
                    c.repaint();
                }
            }
        });

        cmbDirecaoCampoMagnetico.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String dir = (String) cmbDirecaoCampoMagnetico.getSelectedItem();
                direcaoCampoMagnetico = dir.equals("Positivo");
                c.repaint();
            }
        });

        ActionListener timerListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                velocidadeFeixe += (numElectroesFeixe + (30 - 2 * (numElectroesFeixe - 10))) - (difPotencial - 5f); //a última parcela regula a velocidade
                velocidadeFeixeInvertida += difPotencial;
                actualizarLabelsDados();
                c.repaint();
            }
        };
        timer = new Timer(40, timerListener);
        timer.start();
        
        TrataEvento t = new TrataEvento();
        btGravar.addActionListener(t);

        //Dimensão/Posição/Comportamento da janela
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setMinimumSize(new Dimension(900, 600)); //tamanho mínimo da janela
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }

    private void fechar() {
        Object[] opSimNao = {"Sim", "Não"};
        if (JOptionPane.showOptionDialog(JanelaPrincipal.this, "Deseja fechar a aplicação?", "Sair", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opSimNao, opSimNao[1]) == JOptionPane.YES_OPTION) {
            timer.stop(); //pára o timer que controla o movimento do feixo
            dispose();
        }
    }

    private class TrataEvento implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btGravar) {
                String raio =(String)lbraio2.getText();
                String velocidade=(String)lbvel2.getText();
                String fm=(String)lbfm2.getText();
                String campoM = String.valueOf(intensidadeCampoMagnetico);
                    //gravar na base de dados
                    conn.insert(numElectroesFeixe, difPotencial, campoM, velocidade, raio, fm,activarBobines,direcaoCampoMagnetico);

            }
        }
    }

    public class TesteGraphics2d extends JPanel {

        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); //melhora os gráficos

            int y = (int) (panelFeixe.getSize().getHeight() / 2); //meio do eixo dos x do painel/largura do painel
            int x = (int) (panelFeixe.getSize().getWidth() / 2); //meio do eixo dos y do painel/altura do painel
            
            //Desenha feixe
            g2.setColor(Color.RED);
            float[] dash = {5f, numElectroesFeixe + (30 - 2 * (numElectroesFeixe - 10))};
            g2.setStroke(new BasicStroke(5, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1f, dash, velocidadeFeixe));

            //Desenha inha Recta            
            g2.draw(new Line2D.Float(30, y, x, y)); //x1, y1, x2, y2
            //Fim desenha linha recta

            //Desenha linha curva
            //Variaveis auxiliares para calcular a linha curva
            double raio = campoM.calculaRaio() * 100000000; //* 100m para remover os 0's
            double diametro = raio * 2;
            double angulo = campoM.calcularAngulo(raio, cx);
            //Fim Variaveis auxiliares para calcular a linha curva

            if (!activarBobines) { //se as bobines não estiverem activadas o feixe não sofre curvatura
                QuadCurve2D.Double curve = new QuadCurve2D.Double(x - 6, y, x + (x / 2) + (x / 4), y, x * 2 - 30, y); //x1, y1, ctrlx, ctrly, x2, y2
                g2.draw(curve);
            } else {
                if (direcaoCampoMagnetico) {
                    g2.drawArc((int) (x - (diametro / 2)), (int) (y - diametro), (int) diametro, (int) diametro, 270, (int) angulo + 5); //270 é o ponto inicial, 30 é o ponto final onde o 0 começa no ponto inicial (270)
                } else {
                    g2.setStroke(new BasicStroke(5, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1f, dash, velocidadeFeixeInvertida)); //rectifica o movimento da linha
                    g2.drawArc((int) (x - (diametro / 2)), (int) (y), (int) diametro, (int) diametro, 90 - ((int) angulo + 5), (int) angulo + 5); //60 é o ponto inicial, 30 é o ponto final onde o 0 começa no ponto inicial (270)
                }
            }
            //Fim desenha linha curva
            //Fim desenha feixe

            //Desenha cathode (fonte de onde surgem os electrões)
            g2.setColor(Color.DARK_GRAY);
            g2.fillRect(0, y - 20, 35, 40); //x, y(subtrai metade da altura para ficar bem centrado), largura, altura
            //Fim desenha cathode

            //Desenha placas campo eléctrico
            Stroke drawingStrokePlacas = new BasicStroke(13);
            g2.setStroke(drawingStrokePlacas);

            //Placas cima
            int red1 = 255 - (Math.abs((int) difPotencial * (int) 12.5 + 130));
            int blue1 = (Math.abs((int) difPotencial * (int) 12.5 + 130));

            g2.setColor(new Color(red1, 0, blue1)); //red, green, blue
            g2.draw(new Line2D.Float(60, y - 10, 60, y - 35)); //x1, y1, x2, y2
            g2.draw(new Line2D.Float(200, y - 10, 200, y - 35)); //x1, y1, x2, y2
            
            //Placas baixo
            g2.setColor(new Color(red1, 0, blue1)); //red, green, blue
            g2.draw(new Line2D.Float(60, y + 10, 60, y + 35)); //x1, y1, x2, y2
            g2.draw(new Line2D.Float(200, y + 10, 200, y + 35)); //x1, y1, x2, y2
            //Fim desenha placas campo elétrico

            //Desenha bobines
            if (activarBobines) {
                if (direcaoCampoMagnetico) {
                    Image bobine1 = new ImageIcon("bobine_blue.png").getImage();
                    g2.drawImage(bobine1, (x + 62), (y - 62) - 130, null); //62 é o centro da imagem

                    Image bobine2 = new ImageIcon("bobine_red.png").getImage();
                    g2.drawImage(bobine2, (x + 62), (y - 62) + 130, null);
                } else {
                    Image bobine1 = new ImageIcon("bobine_red.png").getImage();
                    g2.drawImage(bobine1, (x + 62), (y - 62) - 130, null); //62 é o centro da imagem

                    Image bobine2 = new ImageIcon("bobine_blue.png").getImage();
                    g2.drawImage(bobine2, (x + 62), (y - 62) + 130, null);
                }
            }
            //Fim desenha bobines

            //Rectângulos de correção
            g2.setColor(new Color(238, 238, 238));
            g2.fillRect(x + 250, y - 1003, 1000, 1000);
            g2.fillRect(x + 250, y + 3, 1000, 1000);
            //Fim rectângulos de correção

            //Recta tangente (3ª linha) do feixe
            g2.setColor(Color.RED);
            g2.setStroke(new BasicStroke(5, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1f, dash, velocidadeFeixe));

            //Variaveis auxiliares para calcular a recta tangente
            double cateto = campoM.calcularCateto(raio, cx);
            double xp1 = x + cx;
            double xcentro = x;
            double yp1 = y - ((int) campoM.calcularY(raio, cateto));
            double ycentro = y - (int) (diametro / 2);
            double xp2 = x + 800;
            double yp2 = campoM.calcularTangente(xcentro, ycentro, xp1, yp1, xp2, xp2);
            yFimCurva = (int)(y-yp1);            
            //Fim Variaveis auxiliares para calcular a recta tangente

            //Desenha recta tangente
            if (activarBobines) {
                if (direcaoCampoMagnetico) {
                    g2.draw(new Line2D.Float((int) xp1, (int) yp1, (int) xp2, (int) yp2)); //x1, y1, x2, y2
                } else {
                    g2.draw(new Line2D.Float((int) xp1, (int) (y + (y - yp1)), (int) xp2, (int) (y + (y - yp2)))); //x1, y1, x2, y2
                }
            }
            //Fim desenha recta tangente do feixe

            //Desenha rectângulo translúcido
            if (mostrarCampoMagnetico && activarBobines) {
                g2.setColor(new Color(0f, 0f, 0f, .2f));//Cor preta com opacidade de 0.2
                g2.fillRect(x, y - 250, 250, 500);
            }
            //Fim desenha rectângulo translúcido
        }
    }
    
    public void actualizarLabelsDados(){
        lbvel2.setText(""+campoM.calcularVelocidade());
        lbraio2.setText(""+campoM.calculaRaio());
        lbfm2.setText(""+campoM.calcularForcaMagnetica());
        if (activarBobines){
            lbx2.setText(""+cx);
            String dir = (String) cmbDirecaoCampoMagnetico.getSelectedItem();
            if (dir.equals("Positivo")){
                lby2.setText(""+yFimCurva);
            } else {
                lby2.setText(""+ (yFimCurva*-1));
            }
        } else {
            lbx2.setText(""+0);
            lby2.setText(""+0);
            lbraio2.setText(""+0);
            lbfm2.setText(""+0);
        }
    }
}

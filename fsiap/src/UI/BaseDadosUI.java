/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package UI;

import Persistencia.ConexaoBD;
import UI.JanelaPrincipal;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hugomonteiro
 */
public class BaseDadosUI extends JDialog{
     
     private static JButton btVisualizar;
     ResultSet dados;
     JanelaPrincipal jal;
     static ConexaoBD conn= new ConexaoBD();
     Vector data_rows = new Vector ();
     Vector nomeColunas= new Vector();
     ResultSetMetaData rsmetadata;
     DefaultTableModel dtm = new DefaultTableModel();
     JTable tabela = new JTable();
    
    public BaseDadosUI (final JFrame framePai){
        super(framePai, "Base de Dados", true);
        
        Container c = getContentPane();
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        tabela.addMouseListener(new java.awt.event.MouseAdapter() {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    int row = tabela.rowAtPoint(evt.getPoint());
                    
                    //obter valores da linha escolhida na tabela
                    String intensidade = (String)tabela.getValueAt(row,0);
                    String ddp = (String)tabela.getValueAt(row,1);
                    String intensidadeCM = (String)tabela.getValueAt(row,2);
                    String velocidade = (String)tabela.getValueAt(row,3);
                    String raio = (String)tabela.getValueAt(row,4);
                    String forcaMagnetica = (String)tabela.getValueAt(row,5);
                    String b=(String)tabela.getValueAt(row,6);
                    boolean bobi;
                    if(b.equals("f"))
                        bobi=false;
                    else
                        bobi=true;
                    
                    String d=(String)tabela.getValueAt(row,7);
                    boolean dire;
                    if(d.equals("f"))
                        dire=false;
                    else
                        dire=true;
                        
                    //criar nova janela
                    JanelaPrincipal jal2 = new JanelaPrincipal(intensidade,ddp,intensidadeCM,velocidade,raio,forcaMagnetica,bobi,dire);
                    //eliminar a anterior
                    jal = (JanelaPrincipal) framePai;
                    jal.dispose();
                }
        });
        
        JScrollPane sc = new JScrollPane(tabela);
        
        btVisualizar = new JButton("Sair");
        p.add(btVisualizar);
        
        c.add(p, BorderLayout.SOUTH);
        getContentPane().add(sc, BorderLayout.CENTER);
        mostra();
        TrataEvento t = new BaseDadosUI.TrataEvento();
        btVisualizar.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(700, 300));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    
    private void mostra()
    {
        try {
            dados= conn.mostrar();
        
            rsmetadata = dados.getMetaData();
        
        int colunas = rsmetadata.getColumnCount();
        for (int i=1;i<colunas; i++)
        {
            nomeColunas.addElement(rsmetadata.getColumnName(i));
        }
        dtm.setColumnIdentifiers(nomeColunas);
        
        while(dados.next())
        {
            data_rows = new Vector();
            for (int j=1 ; j<colunas; j++)
            {
                data_rows.addElement(dados.getString(j));
            }
            dtm.addRow(data_rows);
        }
        tabela.setModel(dtm);
        } catch (SQLException ex) {
            Logger.getLogger(BaseDadosUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }       

    private class TrataEvento implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btVisualizar) {
                dispose();
            }
        }
    }
    
}

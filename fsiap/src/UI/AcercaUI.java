package UI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AcercaUI extends JDialog{
    private JButton btOk;
    
    public AcercaUI (JFrame framePai){
        super(framePai, "Acerca", true);
        
        Container c = getContentPane();
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(10, 40, 10, 40));
        
        JPanel p1 = new JPanel(new GridLayout(5,1));
        p1.setBorder(BorderFactory.createTitledBorder("Autores"));
        JLabel lb1 = new JLabel("Hugo Monteiro - 1050479");
        JLabel lb2 = new JLabel("Fábio Andersen - 1070375");
        JLabel lb3 = new JLabel("Ricardo Sousa - 1070475");
        JLabel lb4 = new JLabel("José Moura - 1080698");
        JLabel lb5 = new JLabel("Vasco Ribeiro - 1090682");
        
        p1.add(lb1);
        p1.add(lb2);
        p1.add(lb3);
        p1.add(lb4);
        p1.add(lb5);
        
        JPanel p2 = new JPanel(new GridLayout(4, 1));
        JLabel lb6 = new JLabel("FSIAP 2013/2014");
        lb6.setHorizontalAlignment(JLabel.CENTER);
        
        JLabel lb7 = new JLabel("<html><center>A carga eletrica num campo elétrico e magnético</center></html>");
        lb7.setPreferredSize(new Dimension(150, 45));
        lb7.setHorizontalAlignment(JLabel.CENTER);
        
        JLabel lb8 = new JLabel("@ISEP");
        lb8.setForeground(new Color(152, 60, 31));
        lb8.setFont(new Font("Arial", Font.BOLD, 18)); 
        lb8.setHorizontalAlignment(JLabel.CENTER);
        
        p2.add(lb6);
        p2.add(lb7);
        p2.add(lb8);
        
        JPanel p2_1 = new JPanel();
        btOk = new JButton("Ok");
        p2_1.add(btOk);
        p2.add(p2_1);
        
        
        p.add(p1, BorderLayout.NORTH);
        p.add(p2, BorderLayout.SOUTH);

        c.add(p);
        
        TrataEvento t = new TrataEvento();
        btOk.addActionListener(t);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setMinimumSize(new Dimension(getWidth(), getHeight()));
        setLocationRelativeTo(null); //coloca a janela no centro do ecra
        setVisible(true);
    }
    
    private class TrataEvento implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btOk) {
               dispose();
            }
        }
    }
}
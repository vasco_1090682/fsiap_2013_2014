/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.lang.Math.*;
import java.text.DecimalFormat;

/**
 *
 * @author 1070375
 */
public class CampoEletroMagnetico extends Feixe {

    // private double difPotencial;
    private double intensidadeCampo;
    private double direcaoCampo;
    
    //constantes
    private static double q = 1.6022 * Math.pow(10, -19);
    private static double mE = 9.109389754 * Math.pow(10, -31);

    public CampoEletroMagnetico(double v, double b, double d) {
        super(v); //diferença de potêncial das placas
        intensidadeCampo = b; //intensidade do campo magnético
        direcaoCampo = d; //direção do campo magnético (positivo/negativo)
    }

    /**
     * @return the intensidadeCampo
     */
    public double getIntensidadeCampo() {
        return intensidadeCampo;
    }

    /**
     * @param intensidadeCampo the intensidadeCampo to set
     */
    public void setIntensidadeCampo(double intensidadeCampo) {
        this.intensidadeCampo = intensidadeCampo;
    }

    /**
     * @return the direcaoCampo
     */
    public double getDirecaoCampo() {
        return direcaoCampo;
    }

    /**
     * @param direcaoCampo the direcaoCampo to set
     */
    public void setDirecaoCampo(double direcaoCampo) {
        this.direcaoCampo = direcaoCampo;
    }

    public double calcularVelocidade(){
        double difP = super.getDifPotencial();
        double res= Math.sqrt(Math.abs((-2 * q * difP) / mE));
        DecimalFormat df = new DecimalFormat ("#.00");
        String aux= df.format(res);
        double ret = Double.parseDouble(aux.replaceAll(",", "."));
        
        return ret;
        
    }
    
    public double calcularForcaMagnetica(){
        double res =q * Math.abs(intensidadeCampo);
        DecimalFormat df = new DecimalFormat ("0.##E0");
        String aux= df.format(res);
      
        double ret = Double.parseDouble(aux.replaceAll(",", "."));
        
        return ret;
      
       
    }
    
    public double calculaRaio() {
       double vel = calcularVelocidade();
        double fm = calcularForcaMagnetica();
       double res =(mE * vel ) / fm;
         DecimalFormat df = new DecimalFormat ("0.##E0");
        String aux= df.format(res);
      
        double ret = Double.parseDouble(aux.replaceAll(",", "."));
        
        return ret;
        
    }
    
    public double calcularCateto(double h, double c){
        return Math.sqrt(h * h - 250 * 250);
    }
    
    public double calcularY(double raio, double cateto){
        return (raio - cateto);
    }
    
    public double calcularTangente(double xcentro, double ycentro, double xp1, double yp1, double xp2, double yp2){
        return ((xp1 - xcentro) / (yp1 - ycentro) * (xp2 - xp1) * -1) + yp1;
    }
    
    public double calcularAngulo(double raio, int c){
        double seno = c / raio;
        double graus = Math.asin(seno);
        return Math.toDegrees(graus);
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author 1070375
 */
public class Feixe {

  private double difPotencial;
  
  public Feixe (double dif)
  {
     difPotencial=dif;
  }

    /**
     * @return the intensidade
     */
  
    
    public double getDifPotencial() {
        return difPotencial;
    }

    /**
     * @param difPotencial the difPotencial to set
     */
    public void setDifPotencial(double difPotencial) {
        this.difPotencial = difPotencial;
    }
}

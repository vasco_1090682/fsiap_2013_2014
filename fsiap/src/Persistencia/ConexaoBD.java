/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Hugomonteiro
 */
package Persistencia;

import java.sql.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hugomonteiro
 */
public class ConexaoBD {

   public Statement stm; // responsavel por realizar as pesquisas na base de dados
   public ResultSet rs; // responsavel por armazenar a pesquisa passad pelo statement
   private String driver = "net.sourceforge.jtds.jdbc.Driver";
   private String caminho = "jdbc:postgresql://localhost:5432/fsiap"; // responsavel pelo caminho
   private String user = "postgres";
   private String pass = "1591kuka";
   public Connection conn; // responsavel por realizar a conexão com a base de dados
   ResultSet dados;
   
   
   // metodos
   public void conexao()
   {
       try
      {
          Class.forName("org.postgresql.Driver");
          conn=DriverManager.getConnection(caminho, user, pass);       
      }catch(SQLException ex)
      {
          JOptionPane.showMessageDialog(null, "Erro de conexão"+ ex.getMessage());
      }
      catch(ClassNotFoundException e){
          JOptionPane.showMessageDialog(null, "Erro de class"+e.getMessage());
      }
   }
   
   
   public void desconexao() //metodo para fechar a conexão a base de dados
   {
       try {
           conn.close();
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Erro ao fechar a conexão. \n Erro:"+ ex.getMessage());
       }
   }
   public void insert(int intensidade, int ddp, String intensidadeCM, String velocidade, String raio, String forcaMagnetica, boolean bobine,boolean dire) 
   {
       try {
            conexao();
            PreparedStatement pst = null;
            stm = conn.createStatement();
            String stm = "INSERT INTO dados(NumeroElectroes, DiferencaPotencial,IntensidadeCM,Velocidade,Raio,ForcaMagnetica,Bobine,Direcao) VALUES(?,?,?,?,?,?,?,?)";
            pst = conn.prepareStatement(stm);
            pst.setInt(1, intensidade);
            pst.setInt(2, ddp);
            pst.setString(3, intensidadeCM);
            pst.setString(4, velocidade);
            pst.setString(5, raio);
            pst.setString(6, forcaMagnetica);
            pst.setBoolean(7, bobine);
            pst.setBoolean(8,dire);
            pst.executeUpdate();

            desconexao(); 
      } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Erro ao gravar dados. \n Erro:"+ ex.getMessage());
       }
   }
   
    public ResultSet mostrar ()
    {
       
       try {
           conexao();
           stm = conn.createStatement();
           dados=stm.executeQuery("select * from dados");
           return dados;
           
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Erro ao obter a informacao. \n Erro:"+ ex.getMessage());
       }
       desconexao();
       return dados;
    }
    
}
